import heapq
import copy
import numpy as np


def __format_list_fel(lst):
    lst = [(u[2], u[1], u[0]) for u in lst]
    return "\n".join([str(u).replace("'", "") for u in lst])


def __format_list_queue(lst):
    return "\n".join([str(u).replace("'", "") for u in lst])


def __get_trucks_queue(lst):
    return "\n".join([u[2] for u in lst])


def __format_waiting_time(dct):
    s = "\n".join(str(u).replace("'", "") for u in dct.items())
    s += "\nAverage = {}".format(
        np.mean(np.array([int(u) for u in dct.values()])))
    return s


def __format_b(b, n):
    return "{}\nAverage: {}".format(b, b / n)


def startDumpTruck(
    n_loader=2,
    n_scale=1,
    n_trucks_in_loader=0,
    n_trucks_in_weigh=0,
    n_trucks_travel=6,
    duration=50,
    load_time_mean=10,
    load_time_std=3,
    weigh_time_mean=15,
    weigh_time_std=4,
    travel_time_mean=30,
    travel_time_std=10
):
    n_trucks = n_trucks_in_loader + n_trucks_in_weigh + n_trucks_travel

    loader_queue = []  # queue
    weigh_queue = []  # queue
    future_event_list = []  # min priority_queue

    def get_load_time(): return max(0, int(
        round(np.random.normal(load_time_mean, load_time_std))))

    def get_weigh_time(): return max(0, int(
        round(np.random.normal(weigh_time_mean, weigh_time_std))))

    def get_travel_time(): return max(0, int(
        round(np.random.normal(travel_time_mean, travel_time_std))))

    trucks_in_loader = []
    trucks_in_scale = []

    for i in range(n_trucks_in_loader):
        if len(trucks_in_loader) < n_loader:
            next_event = (get_load_time(), 'EL', "DT%d" % i)
            heapq.heappush(future_event_list, next_event)
            heapq.heappush(trucks_in_loader, next_event)
        else:
            loader_queue.append(("DT%d" % i, 0))

    for i in range(n_trucks_in_weigh):
        if len(trucks_in_scale) < n_scale:
            next_event = (get_weigh_time(), 'EW', "DT%d" % (n_trucks_in_loader + i))
            heapq.heappush(future_event_list, next_event)
            heapq.heappush(trucks_in_scale, next_event)
        else:
            weigh_queue.append(("DT%d" % (n_trucks_in_loader + i), 0))

    for i in range(n_trucks_travel):
        heapq.heappush(future_event_list, (get_travel_time(), 'ALQ', "DT%d" %
                                           (n_trucks_in_loader + n_trucks_in_weigh + i)))

    n_empty_scale = n_scale - len(trucks_in_scale)
    n_empty_loader = n_loader - len(trucks_in_loader)

    records = []
    bs = 0
    bl = 0
    prev_clock = 0
    clock = 0

    total_waiting_time_loader = dict(
        (("DT%d" % i), 0) for i in range(n_trucks)
    )

    total_waiting_time_weigh = dict(
        (("DT%d" % i), 0) for i in range(n_trucks)
    )

    # initial condition
    records.append(
        {
            'id': "Inisial",
            'clock': clock,
            'lqt': __format_list_queue(loader_queue),
            'lt': len(trucks_in_loader),
            'trucks_in_loader': __get_trucks_queue(trucks_in_loader),
            'wqt': __format_list_queue(weigh_queue),
            'wt': len(trucks_in_scale),
            'trucks_in_weigh': __get_trucks_queue(trucks_in_scale),
            'fel': __format_list_fel(sorted(future_event_list)),
            'bl': __format_b(bl, n_loader),
            'bs': __format_b(bs, n_scale),
            'total_waiting_time_loader': __format_waiting_time(total_waiting_time_loader),
            'total_waiting_time_weigh': __format_waiting_time(total_waiting_time_weigh)
        }
    )

    weighted_sum_len_loader_queue = 0
    weighted_sum_len_weigh_queue = 0

    i = 0
    while True:  # banyak event yang akan diproses
        i += 1

        # proses future event list paling depan, update clocknya
        event = future_event_list[0]
        clock = event[0]

        is_break_now = False
        if clock > duration:
            is_break_now = True
            clock = duration
        else:
            heapq.heappop(future_event_list)

        bl += (clock - prev_clock) * len(trucks_in_loader)
        bs += (clock - prev_clock) * len(trucks_in_scale)

        weighted_sum_len_loader_queue += (clock - prev_clock) * len(loader_queue)
        weighted_sum_len_weigh_queue += (clock - prev_clock) * len(weigh_queue)

        for truck in loader_queue:
            total_waiting_time_loader[truck[0]] += clock - prev_clock

        for truck in weigh_queue:
            total_waiting_time_weigh[truck[0]] += clock - prev_clock

        if not is_break_now:
            if event[1] == 'EL':
                heapq.heappop(trucks_in_loader)

                # langsung masuk ke scale atau masuk antrian weigh kalau tidak ada scale kosong
                if n_empty_scale > 0:
                    # langsung masukkan ke scale, set jadwal kalau scale kelar

                    next_event = (clock + get_load_time(), 'EW', event[2])
                    heapq.heappush(future_event_list, next_event)
                    heapq.heappush(trucks_in_scale, next_event)
                    n_empty_scale -= 1
                else:
                    weigh_queue.append((event[2], clock))

                # isi loader yang kosong, atau catat empty loader kalau tidak ada queue
                if len(loader_queue) > 0:
                    next_to_load = loader_queue.pop(0)

                    next_event = (clock + get_load_time(), 'EL', next_to_load[0])
                    heapq.heappush(future_event_list, next_event)
                    heapq.heappush(trucks_in_loader, next_event)
                else:
                    n_empty_loader += 1
            elif event[1] == 'EW':
                heapq.heappop(trucks_in_scale)

                # set jadwal untuk masuk ALQ setelah travelling
                heapq.heappush(future_event_list,
                            (clock + get_travel_time(), 'ALQ', event[2]))

                # isi scale yang kosong, atau catat empty scale kalau tidak ada queue
                if len(weigh_queue) > 0:
                    next_to_weigh = weigh_queue.pop(0)  # id truck, clock

                    next_event = (clock + get_weigh_time(), 'EW', next_to_weigh[0])
                    heapq.heappush(future_event_list, next_event)
                    heapq.heappush(trucks_in_scale, next_event)
                else:
                    n_empty_scale += 1
            elif event[1] == 'ALQ':
                if n_empty_loader > 0:
                    # langsung masukkan ke loader

                    next_event = (clock + get_load_time(), 'EL', event[2])
                    heapq.heappush(future_event_list, next_event)
                    heapq.heappush(trucks_in_loader, next_event)
                    n_empty_loader -= 1
                else:
                    loader_queue.append((event[2], clock))  # (id truck, clock)

        records.append(
            {
                'id': i,
                'clock': clock,
                'lqt': __format_list_queue(loader_queue),
                'lt': len(trucks_in_loader),
                'trucks_in_loader': __get_trucks_queue(trucks_in_loader),
                'wqt': __format_list_queue(weigh_queue),
                'wt': len(trucks_in_scale),
                'trucks_in_weigh': __get_trucks_queue(trucks_in_scale),
                'fel': __format_list_fel(sorted(future_event_list)),
                'bl': __format_b(bl, n_loader),
                'bs': __format_b(bs, n_scale),
                'total_waiting_time_loader': __format_waiting_time(total_waiting_time_loader),
                'total_waiting_time_weigh': __format_waiting_time(total_waiting_time_weigh)
            }
        )

        prev_clock = clock

        if is_break_now:
            break
    
    n_events = i

    def ___get_avg(total_waiting_time):
        return np.mean([int(u) for u in total_waiting_time.values()])

    summary = {
        'Banyak event': n_events,
        'Clock terakhir': prev_clock,
        '--------------': '--------------',
        'Total waktu sibuk loader': bl,
        'Rata-rata waktu sibuk loader': bl / n_loader,
        'Persentase utilisasi loader': "{} %".format(((bl / n_loader) / prev_clock) * 100),
        '-------------': '--------------',
        'Total waktu sibuk weigh': bs,
        'Rata-rata waktu sibuk weigh': bs / n_scale,
        'Persentase utilisasi weigh': "{} %".format(((bs / n_scale) / prev_clock) * 100),
        '---------------': '--------------',
        'Rata-rata total waktu tunggu truk di antrian loader': ___get_avg(total_waiting_time_loader),
        'Persentase total waktu tunggu truk di antrian loader': "{} %".format((___get_avg(total_waiting_time_loader) / prev_clock) * 100),
        'Rata-rata total waktu tunggu truk di antrian weigh': ___get_avg(total_waiting_time_weigh),
        'Persentase total waktu tunggu truk di antrian weigh': "{} %".format((___get_avg(total_waiting_time_weigh) / prev_clock) * 100),
        '----------------': '--------------',
        'Rata-rata panjang antrian loader / clock': weighted_sum_len_loader_queue / prev_clock,
        'Rata-rata panjang antrian weigh / clock' : weighted_sum_len_weigh_queue / prev_clock
    }

    return records, summary
