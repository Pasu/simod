from flask import Flask, render_template, request
from dump_truck_simulation import startDumpTruck
from wtforms import Form
from wtforms.fields import html5 as h5fields
from wtforms.widgets import html5 as h5widgets

app = Flask(__name__)


class DumpTruckForm(Form):
    duration = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=50)
    nLoader = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=2)
    nScale = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=1)
    nTrucksInLoader = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=0)
    nTrucksInWeigh = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=0)
    nTrucksTravel = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=6)
    loadTimeMean = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=10)
    loadTimeStd = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=3)
    weighTimeMean = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=15)
    weighTimeStd = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=4)
    travelTimeMean = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=30)
    travelTimeStd = h5fields.IntegerField(
        widget=h5widgets.NumberInput(min=0), default=10)


@app.route('/')
def homeindex():
    query = request.args

    if query:
        duration = int(query["duration"])
        n_loader = int(query["nLoader"])
        n_scale = int(query["nScale"])
        n_trucks_in_loader = int(query["nTrucksInLoader"])
        n_trucks_in_weigh = int(query["nTrucksInWeigh"])
        n_trucks_travel = int(query["nTrucksTravel"])
        load_time_mean = int(query["loadTimeMean"])
        load_time_std = int(query["loadTimeStd"])
        weigh_time_mean = int(query["weighTimeMean"])
        weigh_time_std = int(query["weighTimeStd"])
        travel_time_mean = int(query["travelTimeMean"])
        travel_time_std = int(query["travelTimeStd"])

        records, summary = startDumpTruck(
            duration=duration,
            n_loader=n_loader,
            n_scale=n_scale,
            n_trucks_in_loader=n_trucks_in_loader,
            n_trucks_in_weigh=n_trucks_in_weigh,
            n_trucks_travel=n_trucks_travel,
            load_time_mean=load_time_mean,
            load_time_std=load_time_std,
            weigh_time_mean=weigh_time_mean,
            weigh_time_std=weigh_time_std,
            travel_time_mean=travel_time_mean,
            travel_time_std=travel_time_std
        )
    else:
        records = []
        summary = []

    columns = [
        "Id event",
        "Clock t",
        "LQ(t)",
        "L(t)",
        "Trucks in loader",
        "WQ(t)",
        "W(t)",
        "Trucks in weigh",
        "Future event list",
        "BL",
        "BS",
        "Total waiting time loader",
        "Total waiting time weigh"
    ]

    form = DumpTruckForm(query)

    return render_template('homepage.html', records=records, columns=columns, form=form, summary=summary)


if __name__ == "__main__":
    app.run()
