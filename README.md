# Dump Trucks Simulation

## Aplikasi Web
Aplikasi web dari simulasi ini dapat diakses di https://dump-truck-simulation-kel-3.herokuapp.com/

## Prasyarat
Untuk menjalankan aplikasi web ini, pastikan komputer yang digunakan memiliki bahasa pemrograman Python 3 (Python 3 versi 3.7 dan 3.8 dapat digunakan, versi lain belum dicoba) dan Pip, yaitu *package manager* untuk Python (Pip versi 20.0.2 dapat digunakan, versi lain belum dicoba).

Python dapat diunduh melalui link berikut: https://www.python.org/downloads/

Pip sudah ter-*install* sekaligus apabila menggunakan Python 3 versi >=3.4. Walaupun demikian, informasi tentang instalasi Pip apabila dibutuhkan dapat diakses melalui link berikut: https://pip.pypa.io/en/stable/installing/

Versi Python yang tersedia dapat dicek menggunakan *command* berikut:
```
python --version
```

Versi Pip yang tersedia dapat dicek menggunakan *command* berikut:
```
pip --version
```


## Set Up di Local
1. Buat virtual environment dengan command berikut
    ```
    python -m venv env
    ```
2. Aktifkan virtual environment dengan command berikut
    
    Windows:
    ```
    env\Scripts\activate.bat
    ```
    Linux:
    ```
    source env/bin/activate
    ```

3. Install dependency yang diperlukan dengan command berikut
    ```
    pip install -r requirements.txt
    ```
4. Jalankan di local dengan development mode menggunakan command berikut
    ```shell
    FLASK_APP=web_interface.py FLASK_ENV=development flask run
    ```
5. Buka halaman baru pada *browser* perangkat yang digunakan dengan alamat berikut
    ```
    http://127.0.0.1:5000/
    ```
    Jika alamat tersebut tidak tersedia, perhatikan terminal atau *command prompt* yang digunakan untuk langkah 1 sampai 4 dan cari baris dengan tulisan seperti ini.
    ```
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
    ```
    Gunakan alamat yang ditampilkan untuk mengakses halaman simulasi pada *browser* yang digunakan.
6. Jika telah selesai, matikan virtual environment dengan command berikut
    ```
    deactivate
    ```
